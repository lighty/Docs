# Introduction

Vinala is a simple yet powerful framework made by love and PHP ;) , for web developers to make the web programming more ezasyier , faster and wonderful. Created in 10/10/2014 by Youssef HAD, provided nice features to help you in your project.

Vinala employed many cool features :
 
 * Routing engine
 * Template engine calls Atomium
 * Command-line console calls Lumos
 * Caching system
 * Mailing
 * Object Relation Models system (ORM)
 * Query builder built with PDO
 * Schema and migrations system
 * Built-in data (Seeders)
 * Simple setup process
 * MVC structure
 * Plugins
 * Middlewares
 * Events system
 * Exceptions handling
 * Cloud and file storage system
 * A simple links system
 * Authentication system
 * Pagination system
 * Logging system
 * Localisation
 * Aliases
 * Shortcuts functions
 * Testing