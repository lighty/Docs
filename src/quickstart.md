# Quick Start

[![alt return](https://gitlab.com/lighty/Art/raw/master/Resources/signs.png) Main Menu](https://gitlab.com/lighty/Docs/tree/3.3/#index)

- [Installation](#installation)
	- [Install Composer](#install-composer)
	- [Install Vinala](#install-vinala)
- [Apache](#apache)
- [Vinala Requirements](#vinala-requirements)
- [Documentation](#documentation)
- [Development](#development)
- [Versions and Changelog](#versions-and-changelog)
- [Licence](#licence)

## Vinala

Vinala, a PHP Framework for web developers

### Installation

#### Install Composer

Vinala uses `Composer`, You can use Composer to install Vinala and its dependencies.

First, download and install [Composer installer](https://getcomposer.org/)

#### Install Vinala

You can install Vinala via [Composer](https://getcomposer.org/) by running the command of Composer `create-project` in your terminal:

	composer create-project vinala/vinala projet_name --prefer-dist


###  Apache

Vinala comes with `.htaccess` file that uses URL rewriting, if you use Apache, Be sure you have enabled the extension `mod_rewrite`


### Vinala Requirements

Vinala has some system requirements:
* PHP >= 5.5
* Enabling The mod_rewrite Apache extension


### Documentation

We are working on Vinala documentation for every release version, you can take look or update the documentation of next release [here](https://gitlab.com/lighty/Docs/tree/3.3)


### Development

Vinala is open to the contributions of developers, the current released version of Vinala is `3.2` . However developers may instead opt to use the next beta version in [dev](https://github.com/vinala/vinala/tree/dev) branch aliased to `3.3-dev`.


### Versions and Changelog

you can find all Vinala releases [here](https://github.com/vinala/vinala/releases) also the change log [here](https://github.com/vinala/vinala/blob/dev/CHANGES.md)


### Licence

The Vinala framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
